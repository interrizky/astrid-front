import React from 'react'
import { Col } from 'react-bootstrap'
import Axios from 'axios'
import Cookies from 'universal-cookie'

/* Component */
import Activity from '../Components/Activity'

/* Images */
import helpDesk from '../Assets/Images/helpdesk.png'
import aulia from '../Assets/Images/aulia.jpg'

const cookies = new Cookies()

class Widget extends React.Component {
  state = {
    data_widget: [],
  }

  componentDidMount() {
    // https://astrid-back.herokuapp.com/fetchActivities
    // http://localhost:7070/fetchActivities
    Axios.get(`https://astrid-back.herokuapp.com/fetchActivities/${ cookies.get('udatxu').data.result.username }`, {
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then(response => { 
      this.setState({ data_widget: response.data.result }) 
      // console.log(response)
    })
    .catch(error => {
      console.log("Error Fetching Widget Activities")
      console.log(error)
    })    
  }

  render() {
    return(
      <Col className="right-bar col-sm-12 col-md-12 col-lg-3">
          <div id="profile" className="profile">
              <div className="avatar" style={{ textAlign: 'center' }}>
                <img src={ cookies.get('udatxu').data.result.username !== "auliamorina" ? helpDesk : aulia } alt="user-avatar" style={{ 
                  height: '65px', 
                  width: '65px',
                  boxShadow: '0 3px 6px 0 rgba(0, 0, 0, 0.16)',
                  borderRadius: '100px',
                  marginBottom: '2.5%'
                }} />
              </div>
              <div className="text">
                <p style={{ fontSize: "20px", fontWeight: "normal", margin: '0' }}>
                  { cookies.get('udatxu').data.result.name }
                </p>
                <p style={{ fontSize: "16px", fontWeight: "normal", margin: '0' }}>
                  { cookies.get('udatxu').data.result.email }
                </p>
              </div>
          </div>

          <div id="widget" className="wrapper-widget" style={{ marginTop: '30px' }}>
            <div className="judul-widget">
              <p style={{ fontSize: "18px", fontWeight: "normal", textAlign: "center" }}>My Recent Activities</p>
            </div>
            <hr />
            {
              this.state.data_widget.length > 0 ?
                this.state.data_widget.map(data_widget => {
                  return(
                    <Activity key={data_widget.id} date={data_widget.date} action={data_widget.action} />
                  )
                })
              : <p>Data is still empty</p>
            }
          </div>
      </Col>  
    )
  }
}

export default Widget