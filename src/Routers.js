// import React, { Fragment } from 'react'
import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom'
// import { BrowserRouter, Routes, Route } from 'react-router-dom'
// import UserReducer from './Redux/UserReducer'
// import { connect } from 'react-redux'
import Cookies from 'universal-cookie'

/* Components & Layout */
// import Main from './Components/Main'
import Login from './Components/Login'
import Forgot from './Components/Forgot'
import Homepage from './Components/Homepage'
// import HomepageAdmin from './Components/HomepageAdmin'
import ChgPwd from './Components/ChangePwd'
import Request from './Components/Request'
import Contact from './Components/Contact'
import Release from './Components/Release'
import Bukisys from './Components/Subfolder/Bukisys'
import Bukidesk from './Components/Subfolder/Bukidesk'
import Swamitra from './Components/Subfolder/Swamitra'
import Silverlake from './Components/Subfolder/Silverlake'
import CLS from './Components/Subfolder/CLS'
import Display from './Components/Subfolder/Display'
import Chat from './Components/Chat'
import Report from './Components/Report'
import FAQ from './Components/Faq'
import Logout from './Components/Logout'
// import Error from './Components/Error'
import Sidebar from './Layout/Sidebar'
import SidebarAdmin from './Components/SidebarAdmin'
import SidebarSD from './Components/SidebarSD'


class Routers extends React.Component {
  render() {
    const cookies = new Cookies()

    if( !cookies.get('udatxu') ) {
      return(
        <BrowserRouter>
          <Container fluid id="login-bg">
            <Routes>
              <Route exact path="/" element= { <Login /> } />
              <Route exact path="/forgot" element= { <Forgot /> } />
              <Route exact path="/chgpwd" element= { <ChgPwd /> } />
              {/* <Route exact path="/" element= { <Homepage /> } /> */}
              <Route path="*" element={<Navigate to={'/'} />} />
            </Routes> 
          </Container> 
        </BrowserRouter>  
      )
    } else { 
      /* if role di sini */
      if( JSON.parse(cookies.get('udatxu', {doNotParse: "false"})).data.result.role === 'user' && JSON.parse(cookies.get('udatxu', {doNotParse: "false"})).data.result.status === 'active' ) {
        return (
          <BrowserRouter>    
              <Container fluid id="home-bg">
              <Row className="homepage" id="homepage" style={{ display: 'flex', flexDirection: 'row'}}>

                <Col className="left-bar col-sm-12 col-md-12 col-lg-3">
                  <Sidebar />
                </Col>

                <Routes>
                  <Route exact path="/home" element= { <Homepage /> } />
                  <Route exact path="/direct" element= { <Release /> } />
                  <Route exact path="/direct/bukisys" element= { <Bukisys /> } />
                  <Route exact path="/direct/bukidesk" element= { <Bukidesk /> } />
                  <Route exact path="/direct/swamitra" element= { <Swamitra /> } />
                  <Route exact path="/direct/silverlake" element= { <Silverlake /> } />
                  <Route exact path="/direct/cls" element= { <CLS /> } />
                  <Route exact path="/direct/display" element= { <Display /> } />
                  <Route exact path="/request" element= { <Request /> } />
                  <Route exact path="/contact" element= { <Contact /> } />
                  <Route exact path="/chat" element= { <Chat /> } />
                  <Route exact path="/faq" element= { <FAQ /> } />   
                  <Route exact path="/logout" element= { <Logout /> } />                  
                  <Route path="*" element={<Navigate to={'/home'} />} />           
                </Routes>                 

              </Row>
            </Container>
          </BrowserRouter>
        )
      } else if( JSON.parse(cookies.get('udatxu', {doNotParse: "false"})).data.result.role === 'servicedesk' && JSON.parse(cookies.get('udatxu', {doNotParse: "false"})).data.result.status === 'active' ) {
        return(
            <BrowserRouter>
              <Container fluid id="home-bg">
                <Row className="homepage" id="homepage" style={{ display: 'flex', flexDirection: 'row'}}>

                  <Col className="left-bar col-sm-12 col-md-12 col-lg-3">
                    <SidebarSD />
                  </Col>

                  <Routes>
                    <Route exact path="/home" element= { <Homepage /> } />              
                    <Route exact path="/faq" element= { <FAQ /> } />
                    <Route exact path="/logout" element= { <Logout /> } />                  
                    <Route path="*" element={<Navigate to={'/faq'} />} />           
                  </Routes>                                   

                </Row>
              </Container>
            </BrowserRouter>
        )
      } else if( JSON.parse(cookies.get('udatxu', {doNotParse: "false"})).data.result.role === 'admin' && JSON.parse(cookies.get('udatxu', {doNotParse: "false"})).data.result.status === 'active' ) {
        return(
            <BrowserRouter>
              <Container fluid id="home-bg">
                <Row className="homepage" id="homepage" style={{ display: 'flex', flexDirection: 'row'}}>

                  <Col className="left-bar col-sm-12 col-md-12 col-lg-3">
                    <SidebarAdmin />
                  </Col>

                  <Routes>
                    <Route exact path="/home" element= { <Homepage /> } />
                    <Route exact path="/direct" element= { <Release /> } />
                    <Route exact path="/direct/bukisys" element= { <Bukisys /> } />
                    <Route exact path="/direct/bukidesk" element= { <Bukidesk /> } />
                    <Route exact path="/direct/swamitra" element= { <Swamitra /> } />
                    <Route exact path="/direct/silverlake" element= { <Silverlake /> } />
                    <Route exact path="/direct/cls" element= { <CLS /> } />
                    <Route exact path="/direct/display" element= { <Display /> } />
                    <Route exact path="/request" element= { <Request /> } />
                    <Route exact path="/contact" element= { <Contact /> } />                    
                    <Route exact path="/report" element= { <Report /> } />                  
                    <Route exact path="/faq" element= { <FAQ /> } />
                    <Route exact path="/logout" element= { <Logout /> } />                  
                    <Route path="*" element={<Navigate to={'/report'} />} />           
                  </Routes>                 

                </Row>
              </Container>
            </BrowserRouter>          
        )
      } else {
        return(
          <>
            <div>User Tidak Aktif</div>
            <div>{ cookies.get('udatxu', {doNotParse: "false"}).data.result.username }</div>
            <div>{ cookies.get('udatxu', {doNotParse: "false"}).data.result.role }</div>
            <div>{ cookies.get('udatxu', {doNotParse: "false"}).data.result.status }</div>
          </>
        )
      }
    }
  }
}

export default Routers