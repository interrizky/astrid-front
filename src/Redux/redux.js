const redux = require('redux')
const createStore = redux.createStore()

/* 3rd define state global */
const initialState = {
  isLogin: false,
  role: "user"
}

/* 2nd Reducer [ kumpulan action-action yang bertugas merubah state, cuma dia yang bisa ngerubah value dari state ] */
/* Isi state dengan initialState */
const rootReducer = (state = initialState, action) => {
  switch(action.type) {
    case 'CHANGE_LOGIN_STATUS':
      return{
        ...state,
        isLogin: true
      }
    case 'CHANGE_ROLE':
      return{
        ...state,
        role: action.newRole
      }
    default:
      return state
  }
}

/* 1st Store [ Bikin Ini Dulu. Setelah Reducer dibikin, masukin Reducer sebagai Parameter ] */
const store = createStore(rootReducer)
console.log(store.getState())

/* 5th Subscription => memberikan notif perubahan bila value store berubah */
store.subscribe( () => {
  console.log('Store Change : ', store.getState())
})

/* 4th Dispatch atau Action [ define action untuk reducer, function merubah state ditaruh di sini dan action erubahan nilai ditaruh sini ] */
store.dispatch({ type: "CHANGE_LOGIN_STATUS" })
store.dispatch({ type: "CHANGE_ROLE", newRole: "admin" })
console.log(store.getState())