import React, { Fragment } from 'react'
import { Col } from 'react-bootstrap'
// import Cookies from 'universal-cookie'

/* Bootstrap */
import 'bootstrap/dist/css/bootstrap.min.css'

/* Custom CSS */
import '../Assets/CSS/Homepage.css'

/* Layout */
import Widget from '../Layout/Widget'

class Homepage extends React.Component {
  render() {
    // const cookie = new Cookies()
    // console.log(cookie.get('udatxu'))

    return(
      <Fragment>
          <Col id="middle" className="col-sm-12 col-md-12 col-lg-6">
            <div className="crumsbread mb-5" id="crumsbread" style={{ marginTop: '20px'}}>
              <div id="wrapper" className="col-sm-12 col-md-12 col-lg-12">  
                  <div className="box-right">
                    <div className="box-left"></div>  
                    <div className="title">
                      <h4>Welcome</h4>
                      <h4>To Astrid HelpDesk</h4>
                    </div>
                    <div className="body">
                      <p style={{ fontStyle: 'italic' }}>This App helps you to get the faster services from the Service Desk</p>
                    </div>
                  </div>
              </div>                
            </div>
            <div className="row main-content" id="main-content">
              <div className="mb-3">
                <h4>Quick Menu</h4>
              </div>
              <div id="wrapper" className="col-sm-12 col-md-12 col-lg-6">
                <a style={{ color: "black", textDecoration: "none" }} href="/direct/bukisys">
                  <div className="box-right-text-only">
                    <div className="box-left"></div>  
                    <label>Release User AS400/Bukisys</label>
                  </div>
                </a>
              </div>  
              <div id="wrapper" className="col-sm-12 col-md-12 col-lg-6">                  
                <a style={{ color: "black", textDecoration: "none" }} href="/direct/display">
                  <div className="box-right-text-only">
                    <div className="box-left"></div>  
                    <label>Release Display</label>
                  </div>
                </a>
              </div>                      
            </div>    
          </Col>

          <Widget />   
      </Fragment>

    )
  }
}

export default Homepage