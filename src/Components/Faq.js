import React from 'react';
import { Col, Accordion } from 'react-bootstrap'

/* CSS */
import '../Assets/CSS/Homepage.css'

class Faq extends React.Component {
  render() {
    return(
      <Col className="middle col-sm-12 col-md-12 col-lg-9">
        <div className="crumsbread d-flex mb-5" id="crumsbread" style={{ marginTop: '20px', marginRight: '10px'}}>
          <div id="wrapper" className="col-sm-12 col-md-12 col-lg-12">
              <div className="box-right">
                <div className="box-left"></div>  
                <div className="title">
                  <h4>Frequently Asked Question (FAQ)</h4>
                </div>
                <div className="breads">
                  <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                      <li className="breadcrumb-item"><a href="/home" style={{ color: 'black', textDecoration: 'none' }}>Home</a></li>
                      <li className="breadcrumb-item active" aria-current="page">FAQ</li>
                    </ol>
                  </nav>
                </div>
              </div>
          </div>                
        </div>

        <div className="main-content d-flex" id="main-content" style={{ marginRight: '10px' }}>
          <div id="wrapper" className="col-sm-12 col-md-12 col-lg-12">
              <div className="box-right">
                <div className="box-left"></div>                    
                  <Accordion defaultActiveKey="0">
                      <Accordion.Item eventKey="0" className="accordion-info">
                          <Accordion.Header className="question0">
                              <strong>
                                  What is the Astrid Helpdesk?
                              </strong>
                          </Accordion.Header>
                          <Accordion.Body>
                              <p>Astrid Helpdesk is an application that serves to help users throughout Indonesia to get fast and excellent service related to service desk services.</p>
                          </Accordion.Body>
                      </Accordion.Item>

                      <Accordion.Item eventKey="1" className="accordion-info">
                          <Accordion.Header className="question1">
                              <strong>
                                  What services are available at Astrid Helpdesk?
                              </strong>
                          </Accordion.Header>
                          <Accordion.Body>
                              <p>Astrid Helpdesk is an application that serves to help users throughout Indonesia to get fast and excellent service related to service desk services.</p>
                          </Accordion.Body>
                      </Accordion.Item>

                      <Accordion.Item eventKey="2" className="accordion-info">
                          <Accordion.Header className="question2">
                              <strong>
                              How Long is the SLA for this service?
                              </strong>
                          </Accordion.Header>
                          <Accordion.Body>
                              <p>Astrid Helpdesk is an application that serves to help users throughout Indonesia to get fast and excellent service related to service desk services.</p>
                          </Accordion.Body>
                      </Accordion.Item>

                      <Accordion.Item eventKey="3" className="accordion-info">
                          <Accordion.Header className="question3">
                              <strong>
                              Will I get proof that I have made a request?
                              </strong>
                          </Accordion.Header>
                          <Accordion.Body>
                              <p>Astrid Helpdesk is an application that serves to help users throughout Indonesia to get fast and excellent service related to service desk services.</p>
                          </Accordion.Body>
                      </Accordion.Item>

                      <Accordion.Item eventKey="4" className="accordion-info">
                          <Accordion.Header className="question4">
                              <strong>
                              If the service exceeds the SLA, where should I contact?
                              </strong>
                          </Accordion.Header>
                          <Accordion.Body>
                              <p>Astrid Helpdesk is an application that serves to help users throughout Indonesia to get fast and excellent service related to service desk services.</p>
                          </Accordion.Body>
                      </Accordion.Item>
                  </Accordion>
              </div>
          </div>                                                                                                       
        </div>
      </Col>       
    )
  }
}

export default Faq