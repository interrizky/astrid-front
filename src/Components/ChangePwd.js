import React from 'react'
import Axios from 'axios'

/* Component */
import LoginLeft from './LoginLeft'

/* Bootstrap */
import 'bootstrap/dist/css/bootstrap.min.css'

/* Custom CSS */
import '../Assets/CSS/Login.css'

// SweetAlert2
import Swal from 'sweetalert2'
import { EyeOff } from 'react-feather'


class ChangePwd extends React.Component {
  state = {
    Config: {
      // url: 'http://localhost:7070/chgpwd',
      url: 'https://astrid-back.herokuapp.com/chgpwd',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },      
      data: null
    },
    data_uname: localStorage.getItem('username') !== null ? localStorage.getItem('username') : null
  }

  componentDidMount = () => {

  }

  componentDidUpdate = async(prevState, prevProps) => {
    
  }

  render() {
    /* localStorage - keep data alive */
    // let local = localStorage.getItem('username')
    // console.log(local)

    return(
        <div className="row chgpwd-page d-flex h-100 align-items-center">
            <div className="wrapper-kiri col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <LoginLeft />
            </div>
            <div className="wrapper-kanan col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <div className="wrapper-question mx-auto" id="question" style={{ 
                backgroundColor: 'white', 
                justifyContent: 'center', 
                alignItems: 'center', 
                width: '500px', 
                height: '575px',
                // width: '650px', 
                // height: '600px',
                // margin: '5%',
                borderRadius: '15px',
                // postion: 'absolute'
              }}>
                <div className="wrapper-form mx-5" id="wrapper-form" style={{ position: "relative", top: "5%"  }}>
                  <div className="header mb-3">
                    <h4 style={{ textAlign: "center" }}>Forgot Password</h4>
                  </div>
                  <div className="text mb-3">
                    <p style={{ fontSize: "13px" }}>The password consists of 10 (ten) characters and is a combination of characters, letters, and numbers. The maximum duration of password usage is 90 days. It is highly recommended to change the password periodically (weekly / monthly)
                    </p>
                  </div>
                  <form>
                    <div className="form my-4">
                      {
                          this.state.data_uname !== null ?
                          <div className="form-group mb-3">
                              <label htmlFor="inputUserID-label" className="col-form-label" style={{ textAlign: "left" }}>User ID</label>
                              <input type="text" id="UserID" name="UserID" className="form-control" placeholder="Your User ID" aria-label="User ID" aria-describedby="basic-addon1" defaultValue={ this.state.data_uname } disabled/>
                          </div>
                        :
                          null                         
                      }       

                      <div className="form-group mb-3">
                          <label htmlFor="inputJawaban-label" className="col-form-label" style={{ textAlign: "left" }}>New Password</label>
                          <div className="input-group mb-2">
                            <input autoFocus type="password" id="answer1" name="answer1" className="form-control" placeholder="Your Answer 1" aria-label="Answer1" aria-describedby="basic-addon3" autoComplete="off" onKeyPress={ this.movePwdTwo } />
                            <div className="input-group-prepend">
                              <div className="input-group-text"><EyeOff onClick={ this.showHideOne } /></div>
                            </div>
                          </div>
                      </div>   
                      <div className="form-group mb-3">
                          <label htmlFor="inputJawaban-label" className="col-form-label" style={{ textAlign: "left" }}>Confirm Password</label>
                          <div className="input-group mb-2">
                            <input type="password" id="answer2" name="answer2" className="form-control" placeholder="Your Answer 2" aria-label="Answer2" aria-describedby="basic-addon4" autoComplete="off" onKeyUp={ this.typeText } onKeyPress={ this.moveToButton } />
                            <div className="input-group-prepend">
                              <div className="input-group-text"><EyeOff onClick={ this.showHideTwo } /></div>
                            </div>
                          </div>
                      </div>                                                    
                    </div>
                    <div className="button my-4">
                      <div className="input-group mb-3">
                        <button type="button" id="btnSubmit" className="btn-secondary form-control" onClick={ this.clickSubmit } >
                          SUBMIT
                        </button>
                      </div>
                      <div className="input-group mb-3">
                        <button type="button" id="btnBack" className="btn-outline-secondary form-control" onClick={ this.loginBack }>
                          CANCEL
                        </button>           
                      </div>
                    </div>
                  </form>
                </div>                     
              </div>
            </div>
        </div>
    )
  }

  moveToButton = event => {
    if(event.code === 'Enter') {
      document.querySelector('#btnSubmit').focus()
    }    
  }

  movePwdTwo = event => {
    if(event.code === 'Enter') {
      document.querySelector('#answer2').focus()
    }
  }

  typeText = (event) => {
    event.preventDefault()
    if( !document.querySelector('#answer1').value || !event.target.value ) {
      document.querySelector('#btnSubmit').disabled = true       
      document.querySelector('#btnSubmit').className = 'btn-secondary form-control'

    } else {
      document.querySelector('#btnSubmit').disabled = false      
      document.querySelector('#btnSubmit').className = 'btn-warning form-control'
    }
  }

  showHideOne = (event) => {
    event.preventDefault()
    document.querySelector('#answer1').type === 'password' ? document.querySelector('#answer1').type = 'text' : document.querySelector('#answer1').type = 'password'
  } 
  
 showHideTwo = (event) => {
    event.preventDefault()
    document.querySelector('#answer2').type === 'password' ? document.querySelector('#answer2').type = 'text' : document.querySelector('#answer2').type = 'password'
  }   

  loginBack = () => {
    window.location.href = '/login'
  }

  clickSubmit = async(event) => {
    event.preventDefault()

    if( document.querySelector('#answer1').value === '' || document.querySelector('#answer2').value === '' ) {
      Swal.fire({
        title: 'Error!',
        text: 'The New Password Field and Confirm Password Can Not Be Empty',
        icon: 'error',
        confirmButtonText: 'Okay',
        confirmButtonColor: 'Orange',            
      })    
      console.log()            
    } else {
      if( document.querySelector('#answer1').value === document.querySelector('#answer2').value ) {
        const datax = await Axios({
          // url: 'http://localhost:7070/chgpwd',
          url: 'https://astrid-back.herokuapp.com/chgpwd',
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          data: JSON.stringify({ 
            data_uname: document.querySelector('#UserID').value,
            data_pass: document.querySelector('#answer1').value
          })                   
        })

        if( datax.data.result === 'Success' ) {
          let timerInterval
          Swal.fire({
            title: 'Success!',
            text: 'Redirecting to the Login Page',
            icon: 'success',
            confirmButtonText: 'Cool',
            confirmButtonColor: 'orange',
            html: 'Will be redirected in <b></b> milliseconds.',
              timer: 2000,
              timerProgressBar: true,
              didOpen: () => {
                Swal.showLoading()
                const b = Swal.getHtmlContainer().querySelector('b')
                timerInterval = setInterval(() => {
                  b.textContent = Swal.getTimerLeft()
                }, 100)
              },
              willClose: () => {
                clearInterval(timerInterval)
              }
          }).then((result) => {
            /* Read more about handling dismissals below */
            if (result.dismiss === Swal.DismissReason.timer) {
              console.log('I was closed by the timer')
            }

            /* remove it in the LocalStorage (cause of using location.href) */
            localStorage.removeItem('username')

            /* redirecting */
            window.location.href = '/login'
          })
        } else { 
          Swal.fire({
            title: 'Error!',
            text: 'Error Updating! Please Try Again Later',
            icon: 'error',
            confirmButtonText: 'Okay',
            confirmButtonColor: 'Orange',            
          })             
        }
      } else {
        Swal.fire({
          title: 'Error!',
          text: 'The New Password Field and Confirm Password Field Must Be The Same',
          icon: 'error',
          confirmButtonText: 'Okay',
          confirmButtonColor: 'Orange',            
        })          
      }      
    }
  }
}

export default ChangePwd