// MessageParser starter code
class MessageParser {
  constructor(actionProvider, state) {
    this.actionProvider = actionProvider;
    this.state = state;
  }

  parse(message) {
    const lowerCase = message.toLowerCase()

    if (lowerCase.includes('hello')) {
      this.actionProvider.helloHandler()
    }

    if( lowerCase.includes('release') && lowerCase.includes('user') && lowerCase.includes('bukisys') ) {
      const splitString = message.split(': ')
      this.actionProvider.bukisysHandler(splitString[1].toString().toUpperCase())
    }

    if( lowerCase.includes('release') && lowerCase.includes('user') && lowerCase.includes('bukidesk') ) {
      const splitString = message.split(': ')
      this.actionProvider.bukideskHandler(splitString[1].toString().toUpperCase())
    }    

    if( lowerCase.includes('release') && lowerCase.includes('user') && lowerCase.includes('cls') ) {
      const splitString = message.split(': ')
      this.actionProvider.clsHandler(splitString[1].toString().toUpperCase())
    }    
    
    if( lowerCase.includes('release') && lowerCase.includes('user') && lowerCase.includes('silverlake') ) {
      const splitString = message.split(': ')
      this.actionProvider.silverlakeHandler(splitString[1].toString().toUpperCase())
    }    
    
    if( lowerCase.includes('release') && lowerCase.includes('user') && lowerCase.includes('swamitra') ) {
      const splitString = message.split(': ')
      this.actionProvider.swamitraHandler(splitString[1].toString().toUpperCase())
    }        

    if( lowerCase.includes('release') && lowerCase.includes('display') ) {
      const splitString = message.split(': ')
      this.actionProvider.displayHandler(splitString[1].toString().toUpperCase())
    }        
    
    // if (lowerCase.includes('dog')) {
    //   this.actionProvider.handleDog()
    // }    
  }
}

export default MessageParser;