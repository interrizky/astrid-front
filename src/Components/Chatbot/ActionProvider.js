import Axios from 'axios'
import Cookies from 'universal-cookie'
const cookies = new Cookies()

// ActionProvider starter code
class ActionProvider {
  constructor(createChatbotMessage, setStateFunc, createClientMessage) {
    this.createChatbotMessage = createChatbotMessage;
    this.setState = setStateFunc;
    this.createClientMessage = createClientMessage;
  }

  setChatbotMessage = (message) => {
    this.setState(state => (
      {
        ...state,
        messages: [...state.messages, message]
      }
    ))
  }

  helloHandler = () => {
    const message = this.createChatbotMessage("Hello, I'm not self-aware. Luckily. How can I help you?")
    this.setChatbotMessage(message)
  }

  bukisysHandler = (value) => {
    const message = this.createChatbotMessage(`Bukisys Release User ${value} succeed`)
    this.setChatbotMessage(message)

    Axios({  
        method: 'POST',
        // url: 'http://localhost:7070/bukisys_rel',
        url: 'https://astrid-back.herokuapp.com/bukisys_rel',
        headers: {
          'Content-Type': 'application/json'
        },
        data: JSON.stringify({ 
          username: cookies.get('udatxu').data.result.username,
          data_release: `Release User AS400/Bukisys: ${value}`
        })
    })
    .then(response => response)
    .catch(error => {
      console.log("Error Fetching Data")
      console.log(error)
    })    
  }
  
  bukideskHandler = (value) => {
    const message = this.createChatbotMessage(`Bukidesk Release User ${value} succeed`)
    this.setChatbotMessage(message)

    Axios({  
        method: 'POST',
        // url: 'http://localhost:7070/bukidesk_rel',
        url: 'https://astrid-back.herokuapp.com/bukidesk_rel',
        headers: {
          'Content-Type': 'application/json'
        },
        data: JSON.stringify({ 
          username: cookies.get('udatxu').data.result.username,
          data_release: `Release User Bukidesk: ${value}`
        })
    })
    .then(response => response)
    .catch(error => {
      console.log("Error Fetching Data")
      console.log(error)
    })        
  }  

  clsHandler = (value) => {
    const message = this.createChatbotMessage(`CLS Release User ${value} succeed`)
    this.setChatbotMessage(message)

    Axios({  
        method: 'POST',
        // url: 'http://localhost:7070/cls_rel',
        url: 'https://astrid-back.herokuapp.com/cls_rel',
        headers: {
          'Content-Type': 'application/json'
        },
        data: JSON.stringify({ 
          username: cookies.get('udatxu').data.result.username,
          data_release: `Release User CLS: ${value}`
        })
    })
    .then(response => response)
    .catch(error => {
      console.log("Error Fetching Data")
      console.log(error)
    })        
  }  
  
  swamitraHandler = (value) => {
    const message = this.createChatbotMessage(`Swamitra Release User ${value} succeed`)
    this.setChatbotMessage(message)

    Axios({  
        method: 'POST',
        // url: 'http://localhost:7070/swamitra_rel',
        url: 'https://astrid-back.herokuapp.com/swamitra_rel',
        headers: {
          'Content-Type': 'application/json'
        },
        data: JSON.stringify({ 
          username: cookies.get('udatxu').data.result.username,
          data_release: `Release User Swamitra: ${value}`
        })
    })
    .then(response => response)
    .catch(error => {
      console.log("Error Fetching Data")
      console.log(error)
    })        
  }   
  
  silverlakeHandler = (value) => {
    const message = this.createChatbotMessage(`Silverlake Release User ${value} succeed`)
    this.setChatbotMessage(message)

    Axios({  
        method: 'POST',
        // url: 'http://localhost:7070/silverlake_rel',
        url: 'https://astrid-back.herokuapp.com/silverlake_rel',
        headers: {
          'Content-Type': 'application/json'
        },
        data: JSON.stringify({ 
          username: cookies.get('udatxu').data.result.username,
          data_release: `Release User Silverlake: ${value}`
        })
    })
    .then(response => response)
    .catch(error => {
      console.log("Error Fetching Data")
      console.log(error)
    })        
  }    

  displayHandler = (value) => {
    const message = this.createChatbotMessage(`Release Display ${value} succeed`)
    this.setChatbotMessage(message)

    Axios({  
        method: 'POST',
        // url: 'http://localhost:7070/display_rel',
        url: 'https://astrid-back.herokuapp.com/display_rel',
        headers: {
          'Content-Type': 'application/json'
        },
        data: JSON.stringify({ 
          username: cookies.get('udatxu').data.result.username,
          data_release: `Release Display: ${value}`
        })
    })
    .then(response => response)
    .catch(error => {
      console.log("Error Fetching Data")
      console.log(error)
    })          
  }    

//  handleDog() {
//     const message = this.createChatbotMessage("Here's a nice dog picture for you!", {
//       widget: 'dogPicture',
//     })

//     this.setChatbotMessage(message)
//   }
}

export default ActionProvider;