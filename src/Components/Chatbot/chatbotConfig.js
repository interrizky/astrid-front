import { createChatBotMessage } from 'react-chatbot-kit'

/* Widget */
import DogPicture from '../Chatbot/DogPicture.js'

/* Cookies */
import Cookies from 'universal-cookie'

/* Images */
import astrid from '../../Assets/Images/astrid2.jpg'
import user from '../../Assets/Images/helpdesk.png'

const cookies = new Cookies()
const uName = cookies.get('udatxu') ? cookies.get('udatxu').data.result.name : null

const botName = 'Astrid'

const config = {
  initialMessages: [
    createChatBotMessage("Hi, " + uName + "! I'm " + botName + ". You can use this command list to start:"),
    createChatBotMessage("1. Release User Bukisys: <user id>"),
    createChatBotMessage("2. Release User CLS: <user id>"),
    createChatBotMessage("3. Release User Swamitra: <user id>"),
    createChatBotMessage("4. Release User Silverlake: <user id>"),
    createChatBotMessage("5. Release User Bukidesk: <user id>"),
    createChatBotMessage("6. Release Display: <user id>"),
  ],
  botName: botName,
  customStyles: {
    botMessageBox: {
      backgroundColor: 'orange',
    },
    chatButton: {
      backgroundColor: 'orange',
    },
  },
  customComponents: {
    botAvatar: (props) => <img src={astrid} alt="astrid-avatar" style={{ 
      width: '50px',
      height: '50px',
      marginRight: '12.5px',
    }} />,
    userAvatar: (props) => <img src={user} alt="user-avatar" style={{ 
      width: '30px',
      height: '30px',
      marginLeft: '12.5px',
    }} />
  },
  widgets: [
    {
      widgetName: 'dogPicture',
      widgetFunc: (props) => <DogPicture {...props} />,
    },
  ],  
}

export default config
