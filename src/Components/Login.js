import React from 'react'
import Axios from 'axios'
import {connect} from 'react-redux'

/* Component */
import LoginLeft from './LoginLeft'

/* Bootstrap */
import 'bootstrap/dist/css/bootstrap.min.css'

/* Custom CSS */
import '../Assets/CSS/Login.css'

/* Image */
import imgAstrid from '../Assets/Images/astrid2.jpg'

// SweetAlert2
import Swal from 'sweetalert2'

/* Universal Cookie */
import Cookies from 'universal-cookie'
const cookies = new Cookies()


class Login extends React.Component {
  state = {
    Config: {
      // url: 'http://localhost:7070/auth',
      url: 'https://astrid-back.herokuapp.com/auth',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },      
      data: null
    },
    isLogin: true
  }

  // dari Login untuk Props ke Routers.js
  changeLoginStatus = (value) => {
    this.props.onChangeLogin(value)
  }

  clickSubmit = () => {
    if( document.querySelector('#username').value === '' || document.querySelector('#password').value === '' ) {
        Swal.fire({
          title: 'Error!',
          text: 'Should Consider To Change Glasses or Recheck The Field Input',
          icon: 'error',
          confirmButtonText: 'Okay',
          confirmButtonColor: 'Orange',            
        })            
    } else {
      this.setState({ 
        ...this.state,
        Config: {
          ...this.state.Config,
          data: JSON.stringify({ 
            username: document.querySelector('#username').value, 
            password: document.querySelector('#password').value 
          })
        }
      })
    }

  }

  setCookie = (name,value,days) => {
      var expires = "";
      if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
      }
      document.cookie = name + "=" + (value || "")  + expires + "; path=/";
  }     

  componentDidMount = () => {

  }

  componentDidUpdate = async(prevState, prevProps) => {
    if ( prevProps.config !== this.state.Config ) {
      const datax = await Axios(this.state.Config).then(response => response).catch(error => null)

      if( datax !== null ) {
          let timerInterval
          Swal.fire({
            title: 'Success!',
            text: 'Redirecting to Homepage',
            icon: 'success',
            confirmButtonText: 'Cool',
            confirmButtonColor: 'orange',
            html: 'Will be redirected in <b></b> milliseconds.',
              timer: 2000,
              timerProgressBar: true,
              didOpen: () => {
                Swal.showLoading()
                const b = Swal.getHtmlContainer().querySelector('b')
                timerInterval = setInterval(() => {
                  b.textContent = Swal.getTimerLeft()
                }, 100)
              },
              willClose: () => {
                clearInterval(timerInterval)
              }
            }).then((result) => {
              /* Read more about handling dismissals below */
              if (result.dismiss === Swal.DismissReason.timer) {
                  console.log('I was closed by the timer')
              }

              // this.setCookie('udatxu', JSON.stringify(datax), 1)
              cookies.set('udatxu', JSON.stringify(datax), { 
                path: '/',
                maxAge: 86400,
              })

              console.log(cookies.get('udatxu').data.result.role)
              if( cookies.get('udatxu').data.result.role === 'user' ) {
                window.location.href = '/home'                    
              } else if( cookies.get('udatxu').data.result.role === 'admin' ) {
                window.location.href = '/report'
              } else {
                window.location.href = '/faq'
              }
            })
      } else {
          Swal.fire({
            title: 'Error!',
            text: 'Do you want to continue',
            icon: 'error',
            confirmButtonText: 'OKAY',
            confirmButtonColor: 'orange',            
          })          
      }
    }
  } 

  render() { 
    return(
      <div className="row login-page d-flex h-100 align-items-center">
          <div className="wrapper-kiri d-none d-sm-block col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <LoginLeft />
          </div>
          <div className="wrapper-kanan col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div className="wrapper-login mx-auto" id="login" style={{ 
              backgroundColor: 'white', 
              justifyContent: 'center', 
              alignItems: 'center', 
              textAlign: 'center', 
              width: '500px', 
              height: '575px',                 
              // width: '500px', 
              // height: '525px',
              // marginLeft: '25%',
              marginTop: '20px',
              borderRadius: '15px'
            }}>
              <div className="wrapper-image">
                <img src={imgAstrid} alt="LogoLogin" style={{ width: '300px', height: '300px' }} />
                <h4>A S T R I D</h4>
              </div>
              <form>
                <div className="wrapper-form mx-5" id="wrapper-form">
                  <div className="input-group mb-3" style={{ alignItems: "center" }}>
                      <input autoFocus type="text" id="username" name="username" className="form-control" placeholder="Your Username" aria-label="Username" aria-describedby="basic-addon1" />
                  </div>
                  <div className="input-group mb-3" style={{ alignItems: "center" }}>
                      <input type="password" id="password" name="password" className="form-control" placeholder="Your Password" aria-label="Password" aria-describedby="basic-addon1" autoComplete="off" />
                  </div>        
                  <div className="input-group mb-3" style={{ alignItems: "center" }}>
                    <button type="button" id="btnLogin" className="btn-warning form-control" onClick={ this.clickSubmit }>
                      LOGIN
                    </button>        
                  </div>
                  <div className="input-group mb-3" style={{ alignItems: "center", justifyContent: "end" }}>
                    <a href="/forgot" style={{ margin: "0", textDecoration: "none" }}>Forgot Password ?</a>
                  </div>  
                </div> 
              </form>
            </div>                 
          </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return{
    ...state,
    isLogin: state.isLogin
  }
}

const mapDispatchToProps = (dispatch) => {
  return{
    change_status: () => dispatch({ type:'CHANGE_LOGIN_STATE', newValue: true })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)