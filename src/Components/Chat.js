import React, {Fragment} from 'react'
import { Col } from 'react-bootstrap'
import Chatbot from 'react-chatbot-kit'

/* CSS */
// import 'react-chatbot-kit/build/main.css'
import '../Assets/CSS/Chatbot.css'

/* Layout */
import Widget from '../Layout/Widget'

// import config from "./configs/chatbotConfig";
import config from '../Components/Chatbot/chatbotConfig'
// import MessageParser from "./chatbot/MessageParser";
import MessageParser from '../Components/Chatbot/MessageParser'
// import ActionProvider from "./chatbot/ActionProvider";
import ActionProvider from '../Components/Chatbot/ActionProvider'

class Chat extends React.Component {
  render() {
    return (
      <Fragment>
        <Col id="middle" className="col-sm-12 col-md-12 col-lg-6">
          <div className="crumsbread d-flex mb-4" id="crumsbread" style={{ marginTop: '20px', marginRight: '10px'}}>
            <div id="wrapper" className="col-sm-12 col-md-12 col-lg-12">
                <div className="box-right">
                  <div className="box-left"></div>  
                  <div className="title">
                    <h4>Chat With Astrid</h4>
                  </div>
                  <div className="breads">
                    <nav aria-label="breadcrumb">
                      <ol className="breadcrumb">
                        <li className="breadcrumb-item"><a href="/home" style={{ color: 'black', textDecoration: 'none' }}>Home</a></li>
                        <li className="breadcrumb-item active" aria-current="page">Chat With Astrid</li>
                      </ol>
                    </nav>
                  </div>
                </div>
            </div>                
          </div>
  
          <div className="main-content d-flex" id="main-content" style={{ marginTop: '20px', marginRight: '10px'}}>
            <div id="wrapper" className="col-sm-12 col-md-12 col-lg-12">
              <div className="box-right-chatbot">
                    <div className="box-left"></div>
                    <Chatbot
                      config={config}
                      messageParser={MessageParser}
                      actionProvider={ActionProvider}
                    />
              </div>
            </div>
          </div>        
        </Col>
  
        <Widget />
      </Fragment>
    )
  }
}

export default Chat