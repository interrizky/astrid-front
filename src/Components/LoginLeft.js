import React from 'react'

/* Image */
import imgKiri from '../Assets/Images/Logo_KB_Big.png';

const LoginLeft = () => {
  return(
    <div className="div-image mx-auto" style={{
        backgroundImage: `url('${ imgKiri }')`, 
        backgroundSize: "cover", 
        backgroundRepeat: "no-repeat",
        width: '500px', 
        height: '500px'
      }}>
    </div>    
  )
}

export default LoginLeft