import React from 'react'

const Activity = (props) => {
  return(
    <div className="content-widget">
        <div className="list-activity" style={{ fontSize: '12px', fontWeight: "normal" }}>
          <p style={{ margin: "0" }}>{ props.date }</p>
          <p>{ props.action }</p>
          <hr />
        </div>
    </div>                         
  )
}

export default Activity