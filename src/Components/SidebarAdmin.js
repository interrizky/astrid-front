import React from 'react';
import { NavLink } from "react-router-dom"

/* Custom CSS */
import '../Assets/CSS/Homepage.css'

/* Feather Icons */
import { Home, UserCheck, Mail, Phone, HelpCircle, Server, LogOut } from 'react-feather'

class SidebarAdmin extends React.Component {
  render() {
    return(
        <div className="wrapper-sidebar" id="sidebar" style={{ height:'95%' }}>
          <div className="logo mb-3" style={{ textAlign: 'center' }}>
            <img src="https://upload.wikimedia.org/wikipedia/commons/5/5a/KB_Bukopin.svg" alt="Logo-Homepage" style={{ height: '100px', width: '200px' }} />
          </div>                  
          <div className="wrapper-menu">                    
            <ul className="ul-list-menu" style={{ paddingLeft: '25x', paddingRight: '20px' }}>
              <li className="list-menu" style={{ marginBottom: '35px' }}>
                <NavLink to="/home"                 
                style={({ isActive }) =>
                    isActive
                      ? {
                          color: 'orange',
                          textDecoration: 'none', 
                          display: 'flex'
                        }
                      : { 
                          color: 'black',
                          textDecoration: 'none', 
                          display: 'flex'                           
                        }
                  }>
                  <Home style={{ marginRight: '10px' }} />
                  Home
                </NavLink>
              </li>
              <li className="list-menu" style={{ marginBottom: '35px' }}>
                <NavLink to="/direct"                   
                style={({ isActive }) =>
                    isActive
                      ? {
                          color: 'orange',
                          textDecoration: 'none', 
                          display: 'flex'
                        }
                      : { 
                          color: 'black',
                          textDecoration: 'none', 
                          display: 'flex'                           
                        }
                  }
                >
                  <UserCheck style={{ marginRight: '10px' }} />
                  Direct Services
                </NavLink>
              </li>
              <li className="list-menu" style={{ marginBottom: '35px' }}>
                <NavLink to="/request"                   
                style={({ isActive }) =>
                    isActive
                      ? {
                          color: 'orange',
                          textDecoration: 'none', 
                          display: 'flex'
                        }
                      : { 
                          color: 'black',
                          textDecoration: 'none', 
                          display: 'flex'                           
                        }
                  }
                >
                  <Mail style={{ marginRight: '10px' }} />
                  Request to Service Desk
                </NavLink>
              </li>       
              <li className="list-menu" style={{ marginBottom: '35px' }}>
                <NavLink to="/contact"                   
                style={({ isActive }) =>
                    isActive
                      ? {
                          color: 'orange',
                          textDecoration: 'none', 
                          display: 'flex'
                        }
                      : { 
                          color: 'black',
                          textDecoration: 'none', 
                          display: 'flex'                           
                        }
                  }
                >
                  <Phone style={{ marginRight: '10px' }} />
                  Contact Service Desk
                </NavLink>
              </li>                                  
              <li className="list-menu" style={{ marginBottom: '35px' }}>
                <NavLink to="/report"                 
                style={({ isActive }) =>
                    isActive
                      ? {
                          color: 'orange',
                          textDecoration: 'none', 
                          display: 'flex'
                        }
                      : { 
                          color: 'black',
                          textDecoration: 'none', 
                          display: 'flex'                           
                        }
                  }>
                  <Server style={{ marginRight: '10px' }} />
                  Report Activity
                </NavLink>
              </li>           
              <li className="list-menu" style={{ marginBottom: '35px' }}>
                <NavLink to="/faq"                 
                style={({ isActive }) =>
                    isActive
                      ? {
                          color: 'orange',
                          textDecoration: 'none', 
                          display: 'flex'
                        }
                      : { 
                          color: 'black',
                          textDecoration: 'none', 
                          display: 'flex'                           
                        }
                  }>
                  <HelpCircle style={{ marginRight: '10px' }} />
                  FAQ
                </NavLink>
              </li>           
              {/* <li className="list-menu" style={{ marginBottom: '35px' }}>
                <NavLink to="/admin-settings"                 
                style={({ isActive }) =>
                    isActive
                      ? {
                          color: 'orange',
                          textDecoration: 'none', 
                          display: 'flex'
                        }
                      : { 
                          color: 'black',
                          textDecoration: 'none', 
                          display: 'flex'                           
                        }
                  }>
                  <Settings style={{ marginRight: '10px' }} />
                  Settings
                </NavLink>
              </li>                          */}
              <li className="list-menu" style={{ marginBottom: '35px' }}>
                <NavLink to="/logout"                 
                style={({ isActive }) =>
                    isActive
                      ? {
                          color: 'orange',
                          textDecoration: 'none', 
                          display: 'flex'
                        }
                      : { 
                          color: 'black',
                          textDecoration: 'none', 
                          display: 'flex'                           
                        }
                  }>
                  <LogOut style={{ marginRight: '10px' }} />
                  Logout
                </NavLink>
              </li>                                                                                             
            </ul>              
          </div>
        </div>      
    )
  }
}

export default SidebarAdmin