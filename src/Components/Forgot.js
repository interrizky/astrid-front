import React from 'react'
import Axios from 'axios'
import { connect } from 'react-redux'

/* Component */
import LoginLeft from './LoginLeft'

/* Bootstrap */
import 'bootstrap/dist/css/bootstrap.min.css'

/* Custom CSS */
import '../Assets/CSS/Login.css'

// SweetAlert2
import Swal from 'sweetalert2'
import { EyeOff } from 'react-feather'


class Forgot extends React.Component {
  state = {
    Config: {
      // url: 'http://localhost:7070/auth',
      url: 'https://astrid-back.herokuapp.com/auth',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },      
      data: null
    },
    isAvailable: {
      status: false,
      button_status: false,
      question: '',
      answer: ''
    }
  }

  render() { 
    // console.log(this.props)
    console.log(this.state)
    return(
        <div className="row question-page d-flex h-100 align-items-center">
            <div className="wrapper-kiri col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <LoginLeft />
            </div>
            <div className="wrapper-kanan col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <div className="wrapper-question mx-auto" id="question" style={{ 
                backgroundColor: 'white', 
                justifyContent: 'center', 
                alignItems: 'center', 
                width: '500px', 
                height: '575px',
                // marginLeft: '25%',
                // marginTop: '15%',
                borderRadius: '15px',
                // postion: 'absolute',
              }}>
                <div className="wrapper-form mx-5" id="wrapper-form" style={{ position: "relative", top: "5%"  }}>
                  <div className="header mb-5">
                    <h4 style={{ textAlign: "center" }}>Forgot Password</h4>
                  </div>

                  <form>
                    <div className="form my-4">        
                      <div className="form-group mb-3">
                          <label htmlFor="inputUserID-label" className="col-form-label" style={{ textAlign: "left" }}>User ID</label>
                          {
                            this.state.isAvailable.status === false ? 
                            <input autoFocus type="text" id="UserID" name="UserID" className="form-control" placeholder="Your User ID" aria-label="User ID" aria-describedby="basic-addon1" onKeyPress={ this.checkUname } /> 

                            : 
                            <input type="text" id="UserID" name="UserID" className="form-control" placeholder="Your User ID" aria-label="User ID" aria-describedby="basic-addon1" readOnly={true} />
                          }
                          <small id="UserIDHelp" className="form-text text-muted">Press "Enter" After You Have Placed Your UserID</small>
                      </div>
                      <div className="form-group mb-3">
                          <label htmlFor="securityQuestion-label" className="col-form-label" style={{ textAlign: "left" }}>Security Question</label>
                          <input type="text" id="question" name="question" className="form-control" placeholder="Your Question" aria-label="Question" aria-describedby="basic-addon2" defaultValue={this.state.isAvailable.question} readOnly={true} />
                      </div>                        
                      <div className="form-group mb-3">
                          <label htmlFor="inputJawaban-label" className="col-form-label" style={{ textAlign: "left" }}>Answer</label>
                          <div className="input-group mb-2">
                            {
                              this.state.isAvailable.status === false ? 
                                <input type="password" id="answer" name="answer" className="form-control col-md-8 col-lg-8" placeholder="Your Answer" aria-label="Answer" aria-describedby="basic-addon3" autoComplete="off" readOnly={true}/>
                              :
                                <input type="password" id="answer" name="answer" className="form-control col-md-8 col-lg-8" placeholder="Your Answer" aria-label="Answer" aria-describedby="basic-addon3" autoComplete="off" onKeyUp={ this.typeText } onKeyPress={ this.clickToEye } />
                            }
                            
                            <div className="input-group-prepend">
                              <div className="input-group-text"><EyeOff id="eye" onClick={ this.showHide } /></div>
                            </div>
                          </div>
                          <small id="jawabanHelp" className="form-text text-muted">Case Sensitive</small>
                      </div>                            
                    </div>
                    <div className="button my-4">
                      <div className="input-group mb-3">
                        {
                          this.state.isAvailable.button_status === false ? 
                            <button type="button" id="btnSubmit" className="btn-secondary form-control" disabled>
                              SUBMIT
                            </button>
                          :
                            <button type="button" id="btnSubmit" className="btn-warning form-control" onClick={ this.clickSubmit } >
                              SUBMIT
                            </button>                            
                        }                        
                      </div>
                      <div className="input-group mb-3">
                        <button type="button" id="btnBack" className="btn-outline-secondary form-control" onClick={ this.loginBack }>
                          CANCEL
                        </button>           
                      </div>
                    </div>                        
                  </form>
                </div>                     
              </div>
            </div>
        </div>
    )
  }

  clickToEye = (event) => {
    if(event.code === 'Enter') {
      document.querySelector('#btnSubmit').focus()
    }
  }

  typeText = (event) => {
    event.preventDefault()
    if( !event.target.value ){
      console.log("button disable")
      this.setState({
        ...this.state,
        isAvailable: {
          ...this.state.isAvailable,
          button_status: false
        }
      })
    } else {
      console.log("enable")
      this.setState({
        ...this.state,
        isAvailable: {
          ...this.state.isAvailable,
          button_status: true
        }
      })   
    }
  }

 showHide = (event) => {
    event.preventDefault()
    document.querySelector('#answer').type === 'password' ? document.querySelector('#answer').type = 'text' : document.querySelector('#answer').type = 'password'
  }  

  loginBack = () => {
    window.location.href = '/login'
  }

  clickSubmit = () => {
    if( document.querySelector('#answer').value === '' ) {
      Swal.fire({
        title: 'Error!',
        text: 'The Jawaban Field Can Not Be Empty',
        icon: 'error',
        confirmButtonText: 'Okay',
        confirmButtonColor: 'Orange',            
      })    
    } else {
      if( document.querySelector('#answer').value !== this.state.isAvailable.answer ) {
        Swal.fire({
          title: 'Error!',
          text: 'Wrong Answer! Check Your Answer and The Typo!',
          icon: 'error',
          confirmButtonText: 'Okay',
          confirmButtonColor: 'Orange',            
        })    
      } else {
        let timerInterval
        Swal.fire({
          title: 'Success!',
          text: 'Redirecting to the Change Password Page',
          icon: 'success',
          confirmButtonText: 'Cool',
          confirmButtonColor: 'orange',
          html: 'Will be redirected in <b></b> milliseconds.',
            timer: 2000,
            timerProgressBar: true,
            didOpen: () => {
              Swal.showLoading()
              const b = Swal.getHtmlContainer().querySelector('b')
              timerInterval = setInterval(() => {
                b.textContent = Swal.getTimerLeft()
              }, 100)
            },
            willClose: () => {
              clearInterval(timerInterval)
            }
          }).then((result) => {
            /* Read more about handling dismissals below */
            if (result.dismiss === Swal.DismissReason.timer) {
              console.log('I was closed by the timer')
            }

            /* call dispatch dari redux */
            this.props.clickSubmit()

            /* check perubahan data_uname di initialStore global */
            console.log(this.props.data_uname)

            /* put it in the LocalStorage (cause of using location.href) */
            localStorage.setItem('username', document.querySelector('#UserID').value );

            /* User ID dibuat token aja biar safe */
            // window.location.href = `/chgpwd/${ document.querySelector('#UserID') }`

            window.location.href = '/chgpwd'
          })        
      }
    }
  }  

  checkUname = async(event) => {
    if(event.code === 'Enter') {
      event.preventDefault()

      if( document.querySelector('#UserID').value === '' ) {
        Swal.fire({
          title: 'Error!',
          text: 'User ID Field Can Not Be Empty',
          icon: 'error',
          confirmButtonText: 'Okay',
          confirmButtonColor: 'Orange',            
        })    
      } else {
        const datax = await Axios({
          // url: 'http://localhost:7070/checkUname',
          url: 'https://astrid-back.herokuapp.com/checkUname',
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          data: JSON.stringify({ 
            username: document.querySelector('#UserID').value
          })                   
        })

        if(datax.data.result === 'No Results Found') {
          Swal.fire({
            title: 'Oops!',
            text: 'No Result Found',
            icon: 'error',
            confirmButtonText: 'Try Again',
            confirmButtonColor: 'Orange',            
          })  
        } else {
          if( datax.data.message === 'Prohibited' ) {
            Swal.fire({
              title: 'Oops!',
              text: 'User Inactive! Please Contact the Service Desk Hotline For Details!',
              icon: 'error',
              confirmButtonText: 'Okay',
              confirmButtonColor: 'Orange',            
            })   
          } else {
            this.setState({
              ...this.state,
              isAvailable: {
                ...this.state.isAvailable,
                status: true,
                question: datax.data.result.question,
                answer: datax.data.result.answer
              }
            })
          }              
        }

        document.querySelector('#answer').focus()
      }
    }    
  }
}

const mapStateToProps = (state) => {
  return {
    data_uname: state.data_uname
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    clickSubmit: () => dispatch({ type: 'UPDATE_USERNAME', newDataUname: document.querySelector('#UserID').value })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Forgot)