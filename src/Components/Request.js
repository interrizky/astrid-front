import React, { Fragment } from 'react'
import { Col } from 'react-bootstrap'
import Axios from 'axios'

// SweetAlert2
import Swal from 'sweetalert2'

/* Layout */
import Widget from '../Layout/Widget'

/* CSS */
import '../Assets/CSS/Homepage.css'

/* Cookies */
import Cookies from 'universal-cookie'
const cookies = new Cookies()


class Request extends React.Component {
  state = {
    dropdown_inquiry: "",
  }

  render() {
    return(
      <Fragment>
        <Col className="middle col-sm-12 col-md-12 col-lg-6">
          <div className="crumsbread mb-5" id="crumsbread" style={{ marginTop: '20px'}}>
            <div id="wrapper" className="col-sm-12 col-md-12 col-lg-12">  
                <div className="box-right">
                  <div className="box-left"></div>  
                  <div className="title">
                    <h4>Request To Service Desk</h4>
                  </div>
                  <div className="breads">
                    <nav aria-label="breadcrumb">
                      <ol className="breadcrumb">
                        <li className="breadcrumb-item"><a href="/home" style={{ color: 'black', textDecoration: 'none' }}>Home</a></li>
                        <li className="breadcrumb-item active" aria-current="page">Request</li>
                      </ol>
                    </nav>
                  </div>
                </div>
            </div>                
          </div>
          <div id="main-content" className="main-content d-flex">
            <div id="wrapper" className="col-sm-12 col-md-12 col-lg-12">  
                <div className="box-right">
                  <div className="box-left"></div>
                  <div className="form mb-5">
                    <div className="form-group mb-3">
                      <label htmlFor="exampleFormControlSelect1">Request Inquiry</label>
                      <select className="form-control" id="inquiry" onChange={this.handleChange} value={this.state.dropdown_inquiry} >
                        <option value="" disabled>-- Select Inquiry --</option>
                        <option value="resetpassas">Permohonan Reset Password AS400/Bukisys</option>
                        <option value="resetpasshcis">Permohonan Reset Password HCIS</option>
                        <option value="resetpassbuki">Permohonan Reset Password Bukidesk</option>
                      </select>
                    </div>
                    <div className="form-group mb-3">
                      <label htmlFor="exampleFormControlInput1">User ID</label>
                      <input autoFocus type="text" className="form-control" id="userid" placeholder="User ID" />
                    </div>                                            
                  </div>
                  <div className="button">
                    <div className="form-group mb-3" style={{ textAlign: "center" }}>
                      <button type="button" className="btn btn-warning w-75" id="btnRelease" onClick={this.clickRelease}> RELEASE </button>
                    </div>                      
                  </div>                      
                </div>
              </div>                
          </div>   
        </Col>

        <Widget />
      </Fragment>
    )      
  }      
  
  handleChange = (event) => {
    event.preventDefault()
    // console.log(event.target.value)       
    this.setState({
      ...this.state,
      dropdown_inquiry: event.target.value
    }) 
  }

  clickRelease = async(event) => {
    event.preventDefault()
    if( this.state.dropdown_inquiry === "" || document.querySelector('#userid').value === "" || document.querySelector('#userid').value === null || document.querySelector('#userid').value === undefined ) {
      Swal.fire({
        title: 'Error!',
        text: 'Select The Dropdown And Place The User ID On The Field',
        icon: 'error',
        confirmButtonText: 'OKAY',
        confirmButtonColor: 'orange',            
      })    
    } else {
      let inq = ""
      if( document.querySelector('#inquiry').value === "resetpassas" ) {
        inq = "Reset Password AS400/Bukisys"
      } else if( document.querySelector('#inquiry').value === "resetpasshcis" ) {
        inq = "Reset Password HCIS"
      } else {
        inq = "Reset Password Bukidesk"
      }

      const datax = await Axios({  
        method: 'POST',
        // url: 'http://localhost:7070/inquiry',
        url: 'https://astrid-back.herokuapp.com/inquiry',
        headers: {
          'Content-Type': 'application/json'
        },
        data: JSON.stringify({ 
          username: cookies.get('udatxu').data.result.username,
          data_inquiry: `${ inq }: ${document.querySelector('#userid').value}`
        })
      })
      .then(response => response)
      .catch(error => {
        console.log("Error Sending Data Inquiry")
        console.log(error)
      })

      if( datax !== null ) {
        let timerInterval
        Swal.fire({
          title: 'Success!',
          text: 'Auto Refresh',
          icon: 'success',
          confirmButtonText: 'Cool',
          confirmButtonColor: 'orange',
          html: 'Inquiry Succeed! Redirected in <b></b> milliseconds.',
            timer: 2000,
            timerProgressBar: true,
            didOpen: () => {
              Swal.showLoading()
              const b = Swal.getHtmlContainer().querySelector('b')
              timerInterval = setInterval(() => {
                b.textContent = Swal.getTimerLeft()
              }, 100)
            },
            willClose: () => {
              clearInterval(timerInterval)
            }
          }).then((result) => {
            /* Read more about handling dismissals below */
            if (result.dismiss === Swal.DismissReason.timer) {
                console.log('I was closed by the timer')
            }

            window.location.reload()                            
          })        
      }
    }
  }
}

export default Request