import React from 'react'
import { Col, Form } from 'react-bootstrap'
import Axios from 'axios'
import Select from 'react-select'

/* CSS */
import '../Assets/CSS/Homepage.css'
// import DatePicker from 'react-bootstrap-date-picker'

// SweetAlert2
import Swal from 'sweetalert2'

/* HTML to Canvas */
import html2canvas from 'html2canvas'

/* Canvas To PDF */
import jsPDF from 'jspdf'

class Report extends React.Component {
  state = {
    data_user: [],
    data_table: null,
    dropdown_value: ""    
  }

  componentDidMount() {
    /* untuk dropdown */
    Axios({
      method: 'GET',
      // url: 'http://localhost:7070/dropdown_user',
      url: 'https://astrid-back.herokuapp.com/dropdown_user',
      headers: {
        "Content-Type": "application/json",
      }
    })
    .then(response => { 
      this.setState({ 
        ...this.state,
        data_user: response.data.result 
      }) 
      // console.log(response.data.result)
    })
    .catch(error => {
      console.log("Error Fetching Widget Activities")
      console.log(error)
    })
  }

  render() {  
    console.log(this.state)
    // console.log(this.state.data_table)

    return(
      <Col className="middle col-sm-12 col-md-12 col-lg-9">
        <div className="crumsbread d-flex mb-5" id="crumsbread" style={{ marginTop: '20px', marginRight: '10px'}}>
          <div id="wrapper" className="col-sm-12 col-md-12 col-lg-12">
              <div className="box-right">
                <div className="box-left"></div>  
                <div className="title">
                  <h4>Report Activity</h4>
                </div>
                <div className="breads">
                  <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                      <li className="breadcrumb-item"><a href="/home" style={{ color: 'black', textDecoration: 'none' }}>Home</a></li>
                      <li className="breadcrumb-item active" aria-current="page">Report Activity</li>
                    </ol>
                  </nav>
                </div>
              </div>
          </div>                
        </div>

        <div className="main-content d-flex" id="main-content" style={{ marginRight: '10px' }}>
          <div id="wrapper" className="col-sm-12 col-md-12 col-lg-12">
            <div className="box-right">
                  <div className="box-left"></div>

                    <div className="wrapper-filter-userid mb-3">
                      <div className="form-group">
                          <label htmlFor="inputUserID-label" className="col-form-label" style={{ textAlign: "left" }}>User ID</label>
                          {/*                          
                           <select className="form-control dropdown_user" id="dropdown_user" value={this.state.dropdown_value} onChange={this.handleChange}>
                            <option value="" disabled>-- Select User ID --</option>
                            {
                              this.state.data_user.map((data_user, index)=> {
                                return(
                                  <option key={index} value={data_user.username}>{data_user.username}</option>
                                )
                              })                        
                            }
                          
                          </select>
                          */}
                          <Select
                            className="basic-single"
                            classNamePrefix="select"
                            id="select2"
                            name="select2"
                            options={ this.state.data_user.map((data_user) => ({ value: data_user.username, label: data_user.username})) }                            
                            onChange={ this.handleChangeTwo }
                            value={ this.state.dropdown_value }
                            placeholder={ this.state.dropdown_value === '' ? "Place The User ID" : this.state.dropdown_value }
                            isSearchable 
                            isClearable                          
                          />
                      </div>
                    </div>

                    <div className="wrapper-filter-tanggal mb-3">
                      <label htmlFor="date-label" className="col-form-label" style={{ textAlign: "left" }}>Date</label>
                      <div className="wrapper-datepicker d-flex align-items-center">
                        <Form.Control type="date" className='date-first' id="date-first" />
                        <span className="mx-3"> - </span>
                        <Form.Control type="date" className='date-last' id='date-last' />
                      </div>
                    </div>

                    <div className="wrapper-tabel mb-3">
                      <div className="table-responsive">
                        <table className="table table-striped table-hover align-middle">
                          <thead className="text-center">
                              <tr>
                                <th scope="col">#</th>
                                <th scope="col">Datetime</th>
                                <th scope="col">User ID</th>
                                <th scope="col">Request</th>
                                <th scope="col">Status</th>
                              </tr>
                            </thead>
                            <tbody className="text-center">
                              {
                                this.state.data_table === null ?
                                  null
                                : 
                                  this.state.data_table.map((data_table, index) => {
                                    return(
                                      <tr key={index}>
                                        <th scope="row">{index+1}</th>
                                        <td>{data_table.date}</td>
                                        <td>{data_table.username}</td>
                                        <td>{data_table.action}</td>
                                        <td>Sukses</td>
                                      </tr>                                      
                                    )
                                  })
                                // console.log(this.state.data_table)                                                       
                              }
                            </tbody>
                        </table>
                      </div>
                    </div>

                    <div className="row wrapper-button">
                      <div className="button-stacked d-flex justify-content-end">
                        <div className="input-group w-50">
                          <button type="button" id="btnRetrieve" className="btn-warning form-control mx-3" onClick={ this.clickRetrieve }>
                            RETRIEVE
                          </button>                          
                          <button type="button" id="btnExport" className="btn-warning form-control" onClick= { this.clickPrint }>
                            EXPORT
                          </button>                                          
                        </div>     
                      </div>
                    </div>                  
                
            </div>
          </div>
        </div>
      </Col>
    )
  } 

  clickPrint = (e) => {
    e.preventDefault()

    html2canvas(document.querySelector(".table-responsive")).then(canvas => {
        // document.body.appendChild(canvas)

        // const imgData = canvas.toDataURL('image/png')
        // const unit = "pt";
        // const size = "A4"; // Use A1, A2, A3 or A4
        // const orientation = "landscape"; // portrait or landscape

        // const pdf = new jsPDF(orientation, unit, size)

        // const width = pdf.internal.pageSize.getWidth()
        // const height = pdf.internal.pageSize.getHeight()

        // pdf.addImage(imgData, 'PNG', 0, 0, width, height)
        // pdf.save("download.pdf")

        /* fit image to pdf width and height - without pagination */
        const imgData = canvas.toDataURL('image/png');
        const pdf = new jsPDF({
          orientation: 'landscape',
        });
        const imgProps= pdf.getImageProperties(imgData);
        const pdfWidth = pdf.internal.pageSize.getWidth();
        const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
        pdf.addImage(imgData, 'PNG', 0, 0, pdfWidth, pdfHeight);
        pdf.save('download.pdf');        
    });    
  }

  clickRetrieve = async(event) => {
    event.preventDefault()

    if( document.querySelector('#date-first').value === '' || document.querySelector('#date-last').value ==='' ) {
      Swal.fire({
        title: 'Error!',
        text: 'Invalid Date Input',
        icon: 'error',
        confirmButtonText: 'OKAY',
        confirmButtonColor: 'orange',            
      })        
    } else {
      const date = new Date()
      await Axios({
        method: 'POST',
        // url: 'http://localhost:7070/retrieval',
        url: 'https://astrid-back.herokuapp.com/retrieval',
        headers: { 'Content-Type': 'application/json' },
        data: JSON.stringify({
          username: this.state.dropdown_value,
          tgl_awal: document.querySelector('#date-first').value,
          tgl_akhir: date.setDate(date.getDate(document.querySelector('#date-last').value) + 1)
        })
      })
      .then(response => {
        console.log(response)
        this.setState({ 
          ...this.state,
          data_table: response.data.result
        })                 
      })
      .catch(err => null)
    }

  }

  // handleChange = (event) => {
  //   event.preventDefault()
  //   console.log(event.target.value)
  //   this.setState({ 
  //     ...this.state,
  //     dropdown_value: event.target.value
  //   })
  // }

  handleChangeTwo = (event) => {
    // console.log(event)
    this.setState({
      ...this.state,
        dropdown_value: event.value
    })
  }
}

export default Report