import React, { Fragment } from 'react'
import { Col } from 'react-bootstrap'
import Axios from 'axios'

/* Layout */
import Widget from '../../Layout/Widget'

/* CSS */
import '../../Assets/CSS/Homepage.css'

/* SweetAlert2 */
import Swal from 'sweetalert2'

/* Cookies */
import Cookies from 'universal-cookie'
const cookies = new Cookies()

class CLS extends React.Component {
  render() {
    return(
      <Fragment>
        <Col id="middle" className="col-sm-12 col-md-12 col-lg-6">
            <div className="crumsbread mb-5" id="crumsbread" style={{ marginTop: '20px'}}>
              <div id="wrapper" className="col-sm-12 col-md-12 col-lg-12">  
                  <div className="box-right">
                    <div className="box-left"></div>  
                    <div className="title">
                      <h4>Quick Service</h4>
                    </div>
                    <div className="breads">
                      <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                          <li className="breadcrumb-item"><a href="/home" style={{ color: 'black', textDecoration: 'none' }}>Home</a></li>
                          <li className="breadcrumb-item"><a href="/direct" style={{ color: 'black', textDecoration: 'none' }}>Release</a></li>
                          <li className="breadcrumb-item active" aria-current="page">CLS</li>
                        </ol>
                      </nav>
                    </div>
                  </div>
              </div> 
            </div>    

            <div className="row main-content d-flex" id="main-content">
              <div id="wrapper" className="col-sm-12 col-md-12 col-lg-12">                  
                <div className="box-right">
                  <div className="box-left"></div>  
                    <div className="form-group mb-3">
                      <label htmlFor="clsVal" style={{ marginBottom: "10px" }}>User CLS : </label>
                      <input autoFocus type="text" className="form-control" id="clsVal" aria-describedby="textHelp" placeholder="Place the CLS Username Here..." />
                    </div>
                    <div className="form-group mb-3" style={{ textAlign: "center" }}>
                      <button type="button" className="btn btn-warning w-75" id="btnSubmitCLS" onClick={ this.clickSubmit }>SUBMIT</button>
                    </div>
                </div>
              </div>
            </div>

        </Col>

        <Widget />
      </Fragment>

    )
  }

  clickSubmit = async() => {
    if( document.querySelector('#clsVal').value === '') {
      Swal.fire({
        title: 'Error!',
        text: 'The Field Can Not Be Empty',
        icon: 'error',
        confirmButtonText: 'OKAY',
        confirmButtonColor: 'orange',            
      })  
    } else {
      const datax = await Axios({  
        method: 'POST',
        // url: 'http://localhost:7070/cls_rel',
        url: 'https://astrid-back.herokuapp.com/cls_rel',
        headers: {
          'Content-Type': 'application/json'
        },
        data: JSON.stringify({ 
          username: cookies.get('udatxu').data.result.username,
          data_release: `Release User CLS: ${document.querySelector('#clsVal').value}`
        })
      })
      .then(response => response)
      .catch(error => {
        console.log("Error Fetching Widget Activities")
        console.log(error)
      })

      if( datax !== null ) {
        let timerInterval
        Swal.fire({
          title: 'Success!',
          text: 'Auto Refresh',
          icon: 'success',
          confirmButtonText: 'Cool',
          confirmButtonColor: 'orange',
          html: 'Release Succeed! Redirected in <b></b> milliseconds.',
            timer: 2000,
            timerProgressBar: true,
            didOpen: () => {
              Swal.showLoading()
              const b = Swal.getHtmlContainer().querySelector('b')
              timerInterval = setInterval(() => {
                b.textContent = Swal.getTimerLeft()
              }, 100)
            },
            willClose: () => {
              clearInterval(timerInterval)
            }
          }).then((result) => {
            /* Read more about handling dismissals below */
            if (result.dismiss === Swal.DismissReason.timer) {
                console.log('I was closed by the timer')
            }

            window.location.reload()                            
          })        
      }
    }
  }  
}

export default CLS