import React, { Fragment } from 'react';
import { Col } from 'react-bootstrap'
// import { Link } from "react-router-dom"
import Axios from 'axios'

/* Layout */
import Widget from '../Layout/Widget'

/* Cookies */
import Cookies from 'universal-cookie'
const cookies = new Cookies()

class Contact extends React.Component {
  render() {
    return(
      <Fragment>
        <Col id="middle" className="col-sm-12 col-md-12 col-lg-6">
          <div className="crumsbread mb-5" id="crumsbread" style={{ marginTop: '20px'}}>
            <div id="wrapper" className="col-sm-12 col-md-12 col-lg-12">  
                <div className="box-right">
                  <div className="box-left"></div>  
                  <div className="title">
                    <h4>Contact Service Desk</h4>
                  </div>
                  <div className="breads">
                    <nav aria-label="breadcrumb">
                      <ol className="breadcrumb">
                        <li className="breadcrumb-item"><a href="/home" style={{ color: 'black', textDecoration: 'none' }}>Home</a></li>
                        <li className="breadcrumb-item active" aria-current="page">Contact</li>
                      </ol>
                    </nav>
                  </div>
                </div>
            </div>                
          </div>
          <div className="row main-content" id="main-content">
            <div id="wrapper" className="col-sm-12 col-md-12 col-lg-6">
              {/* <a style={{ color: "black", textDecoration: "none" }} href="https://api.whatsapp.com/send?phone=+6287872918333" target="_blank" rel="noopener noreferrer"> */}
              <a href="#!" onClick={this.doubleClickWA} style={{ color: "black", textDecoration: "none" }}>
                <div className="box-right-text-only">
                  <div className="box-left"></div>
                  <div className="isi-box d-flex">
                    <div className="text-whatsapp col-sm-10 col-md-10 col-lg-10">
                      <span>Whatsapp : </span>
                      <br />
                      <span>+62 878-7291-8333</span>
                    </div>
                    <div className="logo-whatsapp col-sm-2 col-md-2 col-lg-2" style={{ textAlign: 'right' }}>
                      <img src="https://png.pngtree.com/element_our/md/20180626/md_5b321c98efaa6.jpg" alt="whatsapp" style={{ width: '50px', height: '50px' }}></img>
                    </div>
                  </div>
                </div>
              </a>
            </div>  
            <div id="wrapper" className="col-sm-12 col-md-12 col-lg-6">
              {/* <a style={{ color: "black", textDecoration: "none" }} href="mailto:service.desk@kbbukopin.com?body=Dear, Service Desk" target="_blank" rel="noopener noreferrer"> */}
              <a href="#!" onClick={this.doubleClickMail} style={{ color: "black", textDecoration: "none" }}>              
                <div className="box-right-text-only">
                  <div className="box-left"></div>
                  <div className="isi-box d-flex">
                    <div className="text-email col-sm-10 col-md-10 col-lg-10">
                      <span>Email : </span>
                      <br />
                      <span>service.desk@kbbukopin.com</span>
                    </div>
                    <div className="logo-email col-sm-2 col-md-2 col-lg-2" style={{ textAlign: 'right' }}>
                      <img src="https://iconarchive.com/download/i75211/cornmanthe3rd/metronome/Communication-email-blue.ico" alt="email" style={{ width: '40px', height: '45px' }}></img>
                    </div>
                  </div>
                </div>
              </a>
            </div>                  
              
          </div>    
        </Col>

        <Widget />
      </Fragment>
    )
  }

  doubleClickWA = (event) => {
    /* buka tab baru untuk whatsapp client */
    window.open('https://api.whatsapp.com/send?phone=+6287872918333', '_blank').focus();

    /* masukkan ke activitiy history */
    Axios({  
      method: 'POST',
      // url: 'http://localhost:7070/inquiry',
      url: 'https://astrid-back.herokuapp.com/inquiry',
      headers: {
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({ 
        username: cookies.get('udatxu').data.result.username,
        data_inquiry: 'Kontak Whatsapp'
      })
    })
    .then(response => response)
    .catch(error => {
      console.log("Error Sending Data Inquiry")
      console.log(error)
    })

    window.location.reload()
  }

  doubleClickMail = (event) => {
    /* buka tab baru untuk mail client */
    window.open('mailto:service.desk@kbbukopin.com?body=Dear, Service Desk', '_blank').focus();   

    /* masukkan ke activitiy history */
    Axios({  
      method: 'POST',
      // url: 'http://localhost:7070/inquiry',
      url: 'https://astrid-back.herokuapp.com/inquiry',
      headers: {
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({ 
        username: cookies.get('udatxu').data.result.username,
        data_inquiry: 'Kontak Email'
      })
    })
    .then(response => response)
    .catch(error => {
      console.log("Error Sending Data Inquiry")
      console.log(error)
    }) 

    window.location.reload()
  }
}

export default Contact