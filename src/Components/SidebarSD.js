import React from 'react';
import { Link } from "react-router-dom"

/* Custom CSS */
import '../Assets/CSS/Homepage.css'

/* Feather Icons */
import { Home, HelpCircle, LogOut } from 'react-feather'

class SidebarSD extends React.Component {
  render() {
    return(
        <div className="wrapper-sidebar" id="sidebar">
          <div className="logo mb-3" style={{ textAlign: 'center' }}>
            <img src="https://upload.wikimedia.org/wikipedia/commons/5/5a/KB_Bukopin.svg" alt="Logo-Homepage" style={{ height: '100px', width: '200px' }} />
          </div>                  
          <div className="wrapper-menu">                    
            <ul className="ul-list-menu">
              <li className="list-menu mb-3">
                <Link to="/homepage" style={{ textDecoration: 'none', display: 'flex' }}>
                  <Home color="black" style={{ marginRight: '10px' }} />
                  <p className="text-menu">Home</p>
                </Link>
              </li>     
              <li className="list-menu mb-3">
                <Link to="/faq" style={{ textDecoration: 'none', display: 'flex' }}>
                  <HelpCircle color="black" style={{ marginRight: '10px' }} />
                  <p className="text-menu">FAQ</p>
                </Link>
              </li>           
              <li className="list-menu mb-3">
                <Link to="/logout" style={{ textDecoration: 'none', display: 'flex' }}>
                  <LogOut color="black" style={{ marginRight: '10px' }} />
                  <p className="text-menu">Logout</p>
                </Link>
              </li>                                                                                             
            </ul>              
          </div>
        </div>      
    )
  }
}

export default SidebarSD