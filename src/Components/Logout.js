import React from 'react'
import { Col } from 'react-bootstrap'
import Cookies from 'universal-cookie'

class Logout extends React.Component {
  render() {
    const cookies = new Cookies()
    // console.log(cookies.get('udatxu'))
    cookies.remove('udatxu', { path: '/' })
    window.location.reload()

    return(
      <Col className="middle col-sm-12 col-md-12 col-lg-9">
        <div className="crumsbread d-flex mb-5" id="crumsbread" style={{ marginTop: '20px', marginRight: '10px'}}>
          <div id="wrapper" className="col-sm-12 col-md-12 col-lg-12">
              <div className="box-right">
                <div className="box-left"></div>  
                <div className="title">
                  <h4>Logout</h4>
                </div>
                <div className="breads">
                  <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                      <li className="breadcrumb-item"><a href="/homepage" style={{ color: 'black', textDecoration: 'none' }}>Home</a></li>
                      <li className="breadcrumb-item active" aria-current="page">Logout</li>
                    </ol>
                  </nav>
                </div>
              </div>
          </div>                
        </div>
      </Col>
    )
  }
}

export default Logout