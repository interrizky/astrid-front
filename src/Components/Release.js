import React, { Fragment } from 'react';
import { Col } from 'react-bootstrap'

/* Layout */
import Widget from '../Layout/Widget'

/* CSS */
import '../Assets/CSS/Homepage.css'

class Release extends React.Component {
  render() {
    return(
      <Fragment>
            <Col className="middle col-sm-12 col-md-12 col-lg-6">
              <div className="crumsbread mb-5" id="crumsbread" style={{ marginTop: '20px'}}>
                <div id="wrapper" className="col-sm-12 col-md-12 col-lg-12">  
                    <div className="box-right">
                      <div className="box-left"></div>  
                      <div className="title">
                        <h4>Direct Services</h4>
                      </div>
                      <div className="breads">
                        <nav aria-label="breadcrumb">
                          <ol className="breadcrumb">
                            <li className="breadcrumb-item"><a href="/home" style={{ color: 'black', textDecoration: 'none' }}>Home</a></li>
                            <li className="breadcrumb-item active" aria-current="page">Direct</li>
                          </ol>
                        </nav>
                      </div>
                    </div>
                </div>                
              </div>

              <div className="row main-content d-flex" id="main-content">
                <div id="wrapper" className="col-sm-6 col-md-6 col-lg-6">                  
                  <a style={{ color: "black", textDecoration: "none" }} href="/direct/bukisys">
                    <div className="box-right-text-only">
                      <div className="box-left"></div>  
                      <label>Release User AS400/Bukisys</label>
                    </div>
                  </a>
                </div>           

                <div id="wrapper" className="col-sm-6 col-md-6 col-lg-6">
                  <a style={{ color: "black", textDecoration: "none" }} href="/direct/bukidesk">
                    <div className="box-right-text-only">
                      <div className="box-left"></div>                    
                      <label>Release User Bukidesk</label>
                    </div>
                  </a>
                </div>  

                <div id="wrapper" className="col-sm-6 col-md-6 col-lg-6">                  
                  <a style={{ color: "black", textDecoration: "none" }} href="/direct/swamitra">
                    <div className="box-right-text-only">
                      <div className="box-left"></div>                    
                      <label>Release User Swamitra</label>
                    </div> 
                  </a>
                </div>           

                <div id="wrapper" className="col-sm-6 col-md-6 col-lg-6">                  
                  <a style={{ color: "black", textDecoration: "none" }} href="/direct/cls">
                    <div className="box-right-text-only">
                      <div className="box-left"></div>                    
                      <label>Release User CLS</label>
                    </div>
                  </a>
                </div>  

                <div id="wrapper" className="col-sm-6 col-md-6 col-lg-6">      
                  <a style={{ color: "black", textDecoration: "none" }} href="/direct/silverlake">
                    <div className="box-right-text-only">
                      <div className="box-left"></div>  
                      <label>Release User Silverlake</label>
                    </div>
                  </a>
                </div>           

                <div id="wrapper" className="col-sm-6 col-md-6 col-lg-6">     
                  <a style={{ color: "black", textDecoration: "none" }} href="/direct/display">             
                    <div className="box-right-text-only">
                      <div className="box-left"></div>                    
                      <label>Release Display</label>
                    </div>
                  </a>
                </div>                                                                                                       
              </div>

            </Col>

            <Widget />
      </Fragment>
    )
  }
}

export default Release