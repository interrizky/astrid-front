import React from 'react'
import ReactDOM from 'react-dom'
import reportWebVitals from './reportWebVitals'
import Routers from './Routers'
// import Homepage from './Components/Homepage'
// import Widget from './Layout/Widget'

import { createStore } from 'redux'
import { Provider } from 'react-redux'
// import StoreData from './Redux/store'


/* State Global */
const initialState = {
  data_uname: null,
  isLogin: false,
  role: null
}

/* Reducer */
const rootReducer = (state = initialState, action) => {
  switch(action.type) {
    case 'UPDATE_USERNAME':
      return{
        ...state,
        data_uname: action.newDataUname
      }
    case 'CHANGE_LOGIN_STATE':
      return{
        ...state,
        isLogin: action.newValue
      }
    case 'GET_ROLE':
      return {
        ...state,
        role: action.newRole
      }

    default:
      return state
  }  
}

/* Store */
const rootStore = createStore(rootReducer)


ReactDOM.render(
  <React.StrictMode>
    <Provider store={ rootStore }> 
      <Routers /> 
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
